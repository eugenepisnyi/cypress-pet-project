module.exports = {
    env: {
        browser: true,
        es2021: true,
    },
    extends: ['prettier', 'plugin:cypress/recommended', 'eslint:recommended', 'plugin:@typescript-eslint/recommended'],
    overrides: [
        {
            env: {
                node: true,
            },
            files: ['.eslintrc.{js,cjs}'],
            parserOptions: {
                sourceType: 'script',
            },
        },
    ],
    parser: '@typescript-eslint/parser',
    parserOptions: {
        ecmaVersion: 'latest',
        sourceType: 'module',
        project: ['./tsconfig.json'],
    },
    plugins: ['cypress', '@typescript-eslint'],
    rules: {
        '@typescript-eslint/no-array-delete': 'error',
        'cypress/no-async-tests': 'error',
        'cypress/no-pause': 'error',
    },
};

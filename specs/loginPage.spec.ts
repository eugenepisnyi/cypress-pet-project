import { loginPage, productPage } from '../pages';
import errorsMessagesData from '../data/errors.messages.data';
import headerData from '../data/header.data';

describe('Login page', () => {
    beforeEach(() => {
        loginPage.visit();
        loginPage.getLoginForm().should('be.visible');
    });

    context('should login with', () => {
        it('standard user', () => {
            loginPage.login(Cypress.env('STANDARD_USER_NAME'), Cypress.env('ROOT_USER_PASSWORD'));
            loginPage.getUrl().should('include', '/inventory.html');
            productPage.getLogo().should('be.visible');
            productPage.getLogo().should('have.text', headerData.logo_text);
        });

        it('performance glitch user', () => {
            loginPage.login(Cypress.env('GLITCH_USER_NAME'), Cypress.env('ROOT_USER_PASSWORD'));
            loginPage.getUrl().should('include', '/inventory.html');
            productPage.getLogo().should('be.visible');
            productPage.getLogo().should('have.text', headerData.logo_text);
        });

        afterEach(() => {
            const menuFragment = productPage.openMenu();
            menuFragment.getMenu().should('be.visible');
            menuFragment.clickLogOutLink();
            loginPage.getLoginForm().should('be.visible');
        });
    });

    context('should NOT login with', () => {
        it('locked user', () => {
            loginPage.login(Cypress.env('LOCKED_USER_NAME'), Cypress.env('ROOT_USER_PASSWORD'));
            loginPage.getErrorMessage().should('be.visible');
            loginPage.getErrorMessage().should('have.text', errorsMessagesData.locked_user);
        });
    });
});

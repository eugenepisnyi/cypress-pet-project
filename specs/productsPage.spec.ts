import 'cypress-each';
import { loginPage, productPage } from '../pages';
import { productsCardData } from '../data/products.data';

describe('Product page', () => {
    const cardData = Object.keys(productsCardData).map((key, index) => [index, key]);

    beforeEach(() => {
        loginPage.visit();
        loginPage.getLoginForm().should('be.visible');
        loginPage.login(Cypress.env('STANDARD_USER_NAME'), Cypress.env('ROOT_USER_PASSWORD'));
        productPage.getLogo().should('be.visible');
    });

    it.each(cardData)('should contains correct product`s card {%d} => %s', (index, cardName) => {
        productPage.getImgSource(index).should('contain', productsCardData[cardName].imgSource);
        productPage.getProductName(index).should('have.text', productsCardData[cardName].name);
        productPage.getProductDescription(index).should('have.text', productsCardData[cardName].description);
        productPage.getProductPrice(index).should('have.text', productsCardData[cardName].price);
        productPage.getAddCartBtn(index).should('be.enabled');
        productPage.getAddCartBtn(index).should('have.text', 'Add to cart');
    });
});

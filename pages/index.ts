import { LoginPage } from './login.page';
import { ProductPage } from './product.page';

export const loginPage = new LoginPage();
export const productPage = new ProductPage();

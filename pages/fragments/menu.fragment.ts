export class MenuFragment {
    private menuElement = '.bm-menu';
    private logOutButton = '#logout_sidebar_link';

    public getMenu() {
        return cy.get(this.menuElement);
    }

    public clickLogOutLink(): void {
        cy.get(this.logOutButton).click();
    }
}

import { MenuFragment } from './fragments/menu.fragment';

export class ProductPage {
    private logo = '.app_logo';
    private menuButton = '#react-burger-menu-btn';

    public getLogo() {
        return cy.get(this.logo);
    }

    public openMenu(): MenuFragment {
        cy.get(this.menuButton).click();

        return new MenuFragment();
    }

    public getImgSource(productIndex) {
        return cy.get('.inventory_item_img img').eq(productIndex).invoke('attr', 'src');
    }

    public getProductName(productIndex) {
        return cy.get('.inventory_item_name').eq(productIndex);
    }

    public getProductDescription(productIndex) {
        return cy.get('.inventory_item_desc').eq(productIndex);
    }

    public getProductPrice(productIndex) {
        return cy.get('.inventory_item_price').eq(productIndex);
    }

    public getAddCartBtn(productIndex) {
        return cy.get('button.btn_primary').eq(productIndex);
    }
}

export class BasePage {
    public visit(): void {
        cy.visit('/');
    }

    public getUrl() {
        return cy.url();
    }
}

import { BasePage } from './base.page';

export class LoginPage extends BasePage {
    private loginForm = '.login-box form';
    private inputUsername = '#user-name';
    private inputPassword = '#password';
    private btnSubmit = '#login-button';
    private errorMessage = '.error-message-container h3';

    public getLoginForm() {
        return cy.get(this.loginForm);
    }

    public getErrorMessage() {
        return cy.get(this.errorMessage);
    }

    public login(username: string, password: string): void {
        cy.get(this.inputUsername).type(username);
        cy.get(this.inputPassword).type(password);
        cy.get(this.btnSubmit).click();
    }
}

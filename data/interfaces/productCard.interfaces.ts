export interface ProductCardInterfaces {
    imgSource: string;
    name: string;
    description: string;
    price: string;
}

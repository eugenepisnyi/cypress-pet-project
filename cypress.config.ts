import { defineConfig } from 'cypress';

export default defineConfig({
    e2e: {
        baseUrl: 'https://www.saucedemo.com/',
        supportFile: false,
        specPattern: 'specs/**/*.spec.{js,jsx,ts,tsx}',
        retries: {
            runMode: 0,
            openMode: 0,
        },
        chromeWebSecurity: false,
        pageLoadTimeout: 30000,
        viewportHeight: 1080,
        viewportWidth: 1920,
        waitForAnimations: true,
        scrollBehavior: 'center',
    },
});

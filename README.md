## Getting started

### Installation

- The node version must be greater than or equal to version 20.11.1
  - You can download the latest version of node [here](https://nodejs.org/en/download)
- Install dependencies: `npm install`

## Configuration:

### Set environment variable:

Create `cypress.env.json` file at the root of the directory with the structure:

- There are different ways to set environment variables. [here](https://docs.cypress.io/guides/guides/environment-variables#Setting)

```
{
  LOCKED_USER_NAME=user_name
  GLITCH_USER_NAME=user_name
  STANDARD_USER_NAME=user_name
  ROOT_USER_PASSWORD=user_password
}
```

You can get credentials of users from https://www.saucedemo.com/

## Running tests

### Running the Example Test in UI Mode

Run your tests with UI Mode for a better developer experience with time travel debugging:

- `npm run cy:open`

### CLI options

| Parameter |  Type   | Description                                                                             |
| :-------- | :-----: | :-------------------------------------------------------------------------------------- |
| --browser | String  | The "browser" argument can be set to `chrome`, `edge`, `firefox`. Default = `electron`. |
| --headed  | Boolean | Displays the browser instead of running headlessly. Default = `false`.                  |
| --spec    | String  | Specify the spec files to run                                                           |

### Spec execution

Runs Cypress tests to completion. By default, cypress run will run all tests headlessly.

- You can see all command-line commands [here](https://docs.cypress.io/guides/guides/command-line#Commands)

- Run **`npm run cy:run:chrome -- --headed"`**
- Run **`npm run cy:run:chrome -- --headed --spec "specs/yourtest_1.spec.ts"`**
